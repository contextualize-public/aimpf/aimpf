"""
This is a command-line interface (CLI) script that pulls data from db38 and stores it in an Excel file. The original script is developed and maintained by Kathryn Kelly from Georgia Tech and the code is wrapped by Chen Chen from Contextualize to make it a CLI script.
"""

import argparse
import logging
import sys
import warnings

from aimpf import __version__

# Specific imports for this script
import os
from datetime import datetime
from pycarta.auth import CartaAgent
from aimpf.parcode.db38pulling.kkelly import Db38Kate
from dotenv import load_dotenv

__author__ = "Chen Chen"
__copyright__ = "Contextualize LLC"
__license__ = "MIT"

_logger = logging.getLogger(__name__)


# ---- Python API ----
# The functions defined in this section can be imported by users in their
# Python scripts/interactive interpreter, e.g. via
# `from aimpf.skeleton import fib`,
# when using this Python module as a library.

def db38pulling(
        experiment_list,
        experiment_file,
        master_folder,
        exclude_motion_data,
        username,
        password
    ):
    """
    Pull data from db38 and store it in an Excel file.

    Args: Self-explanatory

    Returns: None

    TODO: Switch over to pycarta session agent to manage login. pycarta has added support for profiles and, eventually, will add support for SAML 2.0 auth. However, this should not hold up the current MR. In the future, add something like,
    
    if username is not None and password is not None:
        pycarta.login(username=username, password=password)
        agent = pycarta.get_agent()
    else:
        pycarta.login(
            username=os.getenv("SBGSECRET_CARTA_USERNAME"),
            password=os.getenv("SBGSECRET_CARTA_PASSWORD"),
        )
        agent = pycarta.get_agent()
    load_dotenv()
    """
    # Load environment variables, in development, you can set them in a .env file, while on seven bridges, you can set them as the environment variables.
    if username is not None and password is not None:
        agent = CartaAgent(
            username=username,
            password=password
        )
    else:
        agent = CartaAgent(
            username=os.getenv("SBGSECRET_CARTA_USERNAME"),
            password=os.getenv("SBGSECRET_CARTA_PASSWORD")
        )
    kate = Db38Kate(agent, namespace="aimpf", service="mysql")
    
    kate.main(
        experiment_file=experiment_file,
        experiment_list=experiment_list,
        master_folder=master_folder,
        motion_toggle=(not exclude_motion_data)
    )

# ---- CLI ----
# The functions defined in this section are wrappers around the main Python
# API allowing them to be called directly from the terminal as a CLI
# executable/script.

def parse_args(args):
    """
    Parse command line parameters and return the arguments as a namespace object.

    This function processes command line parameters for pulling data from the db38 database and storing it in an Excel file. The function provides several options for configuring the behavior of the script, including specifying experiment names either directly as a list or from a file, setting verbosity levels, toggling motion analysis, and specifying a master folder for output.

    If neither `--experiment-list` nor `--experiment-file` is provided, the function defaults to using `--experiment-list` with the value "Franuc_CRS_40_ROT_50IPT". The master folder name defaults to "Backup-of-mysql" if not specified. The motion toggle is enabled by default.

    Args:
        args (List[str]): Command line parameters as a list of strings (e.g., ["--help"]).

    Returns:
        argparse.Namespace: A namespace object containing the parsed command line arguments.

    Raises:
        SystemExit: If required arguments are missing or incorrect arguments are provided.
    """
    # Create the parser and add basic arguments
    parser = argparse.ArgumentParser(
        description="Pulling data from db38 and store it in an Excel file."
    )

    parser.add_argument(
        "--version",
        action="version",
        version=f"aimpf {__version__}",
    )

    parser.add_argument(
        "-v",
        "--verbose",
        dest="loglevel",
        help="Set loglevel to INFO",
        action="store_const",
        const=logging.INFO,
    )
    
    parser.add_argument(
        "-vv",
        "--very-verbose",
        dest="loglevel",
        help="Set loglevel to DEBUG",
        action="store_const",
        const=logging.DEBUG,
    )

    # Create a mutually exclusive group for experiment_list and experiment_file
    group = parser.add_mutually_exclusive_group()
    group.add_argument(
        "-l",
        "--experiment-list", 
        dest="experiment_list",
        help="List of experiment names, separated by comma",
        type=str,
        metavar="STR"
    )
    group.add_argument(
        "-f",
        "--experiment-file", 
        dest="experiment_file",
        help="Filename containing experiment names, one per line",
        type=str,
        metavar="STR"
    )

    parser.add_argument(
        "-m",
        "--master-folder", 
        dest="master_folder",
        help="Master folder name, default is 'Backup-of-mysql'",
        type=str,
        metavar="STR",
        default="Backup-of-mysql"
    )
    
    parser.add_argument(
        "-e",
        "--exclude-motion-data",
        dest="exclude_motion_data",
        help="Exclude motion data. To include it, use '--no-exclude-motion-data'.",
        action=argparse.BooleanOptionalAction,
        type=bool,
        metavar="BOOL",
        default=False,
    )

    parser.add_argument(
        "-u",
        "--username",
        dest="username",
        help="Optional username for authentication",
        type=str,
        metavar="STR",
        default=None
    )

    parser.add_argument(
        "-p",
        "--password",
        dest="password",
        help="Optional password for authentication",
        type=str,
        metavar="STR",
        default=None
    )

    return parser.parse_args(args)


def setup_logging(loglevel):
    logformat = "[%(asctime)s] %(levelname)s:%(name)s:%(message)s"
    logging.basicConfig(
        level=loglevel, stream=sys.stdout, format=logformat, datefmt="%Y-%m-%d %H:%M:%S"
    )


def main(args):
    args = parse_args(args)
    setup_logging(args.loglevel)
    _logger.debug(f"Started pulling data at {datetime.now().isoformat()}")
    db38pulling(
        experiment_list=args.experiment_list,
        experiment_file=args.experiment_file,
        master_folder=args.master_folder,
        exclude_motion_data=args.exclude_motion_data,
        username=args.username,
        password=args.password
        )
    _logger.info(f"Finished pulling data at {datetime.now().isoformat()}")


def run():
    try:
        main(sys.argv[1:])
    except Exception as e:
        _logger.error(f"An unexpected error has occurred and {sys.argv[0]} has been forced to abort.")
        sys.exit(1)



if __name__ == "__main__":
    run()
