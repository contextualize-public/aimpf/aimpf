
import os
from pycarta.auth import CartaAgent
from aimpf.dispatcher.mysql import Db38

import csv
import os
import pathlib
import pandas as pd

class Db38Kate(Db38):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._label = "db38"

    def select_data_func(
        self,
        experiment_list=None,
        experiment_file=None,
        master_folder="Backup-of-mysql",
        motion_toggle=True
        ):
        """
        Select the data function with options for providing an experiment list, reading from a file, specifying a master folder, and toggling motion data.

        Args:
            experiment_list (List[str] or str, optional): A list of experiment names or a comma-separated string. 
                Defaults to "Franuc_CRS_40_ROT_50IPT".
            experiment_file (str, optional): Filename containing experiment names, one per line.
            master_folder (str, optional): Name of the master folder. Defaults to "Backup-of-mysql".
            motion_toggle (bool, optional): Enable or disable motion data. Defaults to True.

        Returns:
            tuple: A tuple containing the experiment list, master folder, and motion toggle.
        """
        # Set default Experiment_List if neither experiment_list nor experiment_file is provided
        if experiment_list is None and experiment_file is None:
            Experiment_List = ["Franuc_CRS_40_ROT_50IPT"]
        elif experiment_file:
            # Read the experiment names from the provided file and flatten to a list of strings
            with open(experiment_file, newline='', encoding='utf-8-sig') as f:
                reader = csv.reader(f)
                Experiment_List = [row[0].strip() for row in reader if row != []]
        else:
            # If experiment_list is a string, split it into a list using ',' as a delimiter
            if isinstance(experiment_list, str):
                Experiment_List = [item.strip() for item in experiment_list.split(',')]
            else:
                Experiment_List = experiment_list.strip()

        # Ensure MasterFolder has a default value if not provided
        MasterFolder = master_folder

        # Ensure MotionToggle is True by default if not explicitly set
        MotionToggle = motion_toggle

        return Experiment_List, MasterFolder, MotionToggle

    def main_func(self, ExpName, MasterFolder, MotionToggle):
        # ======== MAKE SURE EXP NAME IS STRING ========
        if not isinstance(ExpName,str):
            ExpName = str(ExpName)

        # ======== DATA STORAGE FOLDER ========
        path = pathlib.Path().resolve()
        if MasterFolder == "":
            if not os.path.isdir(path):
                os.mkdir(path)
        else:
            path = os.path.join(path, MasterFolder)
            if not os.path.isdir(path):
                os.mkdir(path) 

        out_path = os.path.join(path,f"{ExpName}.xlsx")
        print(f"Saving to {out_path}.")
        writer = pd.ExcelWriter(out_path, engine='xlsxwriter')

        # ======== PULLING DATA ========
        database = "ProcessData"  # [cc] Database name

        DataItemTables = [
            "BaslerCameraData_Fronius", 
            "PowerSupplyData_Fronius", 
            "IRCameraData_Fronius", 
            "MaintenanceData_Fronius"
        ] + (["PositionData_Fanuc"] if MotionToggle else [])

        for Tables in DataItemTables:
            if Tables == "PowerSupplyData_Fronius":
                Data_IDs = [
                    "ACTUAL_CURRENT", 
                    "ACTUAL_VOLTAGE", 
                    "ACTUAL_POWER", 
                    "ACTUAL_WFS",
                    "DISPLAY_ENERGY"
                ]
                
            if Tables == "IRCameraData_Fronius":
                Data_IDs = ["Bx1"] #,"Sp1","Sp2","Sp3","Sp4","Sp5"]

            if Tables == "BaslerCameraData_Fronius":
                Data_IDs = ["CTWD (mm)"]

            if Tables == "PositionData_Fanuc":
                Data_IDs = ["X", "Y", "Z", "W", "P", "R" ]

            if Tables == "MaintenanceData_Fronius":
                Data_IDs = [
                    "ACTUAL_WELDINGTIME",
                    "JOBNAME",
                    "JOBNUMBER"
                ]

            for data_item_id in Data_IDs:
                data = self.list(
                    database=database, 
                    table=Tables,
                    where=f"ExperimentLabel={ExpName},dataItemId={data_item_id}"
                )
                columns = self.list_columns(
                    database=database, 
                    table=Tables
                )['columns']
                dataframe = pd.DataFrame(data, columns=columns)

                # Format the datetime column to the desired format, which doesn't really matter
                dataframe["dateTime"] = pd.to_datetime(dataframe["dateTime"], errors='coerce')        
                dataframe["dateTime"] = dataframe["dateTime"].dt.strftime('%Y-%m-%d %H:%M:%S')
                
                if Tables == "IRCameraData_Fronius":
                    dataframe = dataframe[["dateTime", "value"]]
                else:
                    dataframe = dataframe[["dateTime", "value", "BeadNumber"]]
                # ======== SAVING EXCEL FILE ========
        
                dataframe.to_excel(writer, sheet_name=f"{data_item_id}", index=False)

                # ======== SAVING GRAPHS IN FILE ========

                if Tables != "MaintenanceData_Fronius":
                    #beads
                    beadtoggle=True
                    rec_on=0
                    vals=[]
                    beadnum=1
                    timer=[0]
                    for i in range(len(dataframe)):
                        if not beadtoggle:
                            if not dataframe['value'][i]==0:
                                rec_on=i
                                beadtoggle=True
                        if beadtoggle:
                            if i == 0: 
                                vals.append(dataframe['value'][i])
                                timer.append(0)
                            elif pd.to_datetime(dataframe['value'][i]) != 0: 
                                difftime=pd.to_datetime(dataframe['dateTime'][i])-pd.to_datetime(dataframe['dateTime'][rec_on])
                                difftime=difftime.total_seconds()
                                if difftime != 0:
                                    vals.append(dataframe['value'][i])
                                    timer.append(difftime)

                            if dataframe['value'][i]==0:
                                #graph
                                #tempdf=pd.DataFrame({'avgvals':avgvals})
                                tempdf=pd.DataFrame(list(zip(timer, vals)), columns=['Time','Values'])
                                tempdf.to_excel(writer, sheet_name=f"{data_item_id}_GRAPHS", startrow=rec_on, index=False)
                                wb=writer.book
                                ws=writer.sheets[f"{data_item_id}_GRAPHS"]

                                chart=wb.add_chart({'type':'line'})
                                chart.add_series({'values':f'={data_item_id}_GRAPHS!$B${rec_on+2}:$B${i}','categories':f'={data_item_id}_GRAPHS!$A${rec_on+2}:$A${i}'})
                                chart.set_title({'name':f"Bead Number {beadnum}"})
                                ws.insert_chart(f'E{((beadnum-1)*20)+1}',chart)
                                """ chart=plt.figure()
                                plt.plot(tempdf)
                                plt.title(f"Bead Number {beadnum}")
                                plt.xlabel("Time (seconds)")
                                plt.ylabel("Value")
                                ws.add_chart(chart,f"E{beadnum*20}")
                                wb.save() """

                                #reset
                                beadtoggle=False
                                vals=[]
                                beadnum=beadnum+1
                                timer=[0]

                print(f"Data for {data_item_id} in {ExpName} saved.")

        writer.close()

    def main(
            self,
            experiment_list=None,
            experiment_file=None,
            master_folder="Backup-of-mysql",
            motion_toggle=True
        ):
        arr1, arr2, arr3 = self.select_data_func(
            experiment_list=experiment_list,
            experiment_file=experiment_file,
            master_folder=master_folder,
            motion_toggle=motion_toggle
        )
        
        for i in range(len(arr1)):
            print(f"=======\nRunning experiment {i+1}/{len(arr1)}.\n=======")
            self.main_func(arr1[i], arr2, arr3)
        print("Done! :)\n")
