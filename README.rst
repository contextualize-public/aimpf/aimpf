.. These are examples of badges you might want to add to your README:
   please update the URLs accordingly

    .. image:: https://api.cirrus-ci.com/github/<USER>/aimpf.svg?branch=main
        :alt: Built Status
        :target: https://cirrus-ci.com/github/<USER>/aimpf
    .. image:: https://readthedocs.org/projects/aimpf/badge/?version=latest
        :alt: ReadTheDocs
        :target: https://aimpf.readthedocs.io/en/stable/
    .. image:: https://img.shields.io/coveralls/github/<USER>/aimpf/main.svg
        :alt: Coveralls
        :target: https://coveralls.io/r/<USER>/aimpf
    .. image:: https://img.shields.io/pypi/v/aimpf.svg
        :alt: PyPI-Server
        :target: https://pypi.org/project/aimpf/
    .. image:: https://img.shields.io/conda/vn/conda-forge/aimpf.svg
        :alt: Conda-Forge
        :target: https://anaconda.org/conda-forge/aimpf
    .. image:: https://pepy.tech/badge/aimpf/month
        :alt: Monthly Downloads
        :target: https://pepy.tech/project/aimpf
    .. image:: https://img.shields.io/twitter/url/http/shields.io.svg?style=social&label=Twitter
        :alt: Twitter
        :target: https://twitter.com/aimpf

.. image:: https://img.shields.io/badge/-PyScaffold-005CA0?logo=pyscaffold
    :alt: Project generated with PyScaffold
    :target: https://pyscaffold.org/

|

=====
aimpf
=====


    Add a short description here!


A longer description of your project goes here...


.. _pyscaffold-notes:

Note
====

This project has been set up using PyScaffold 4.5. For details and usage
information on PyScaffold see https://pyscaffold.org/.
