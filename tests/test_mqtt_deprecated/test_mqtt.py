import os
import pytest

from pprint import pprint
from dotenv import load_dotenv
from pycarta.auth import AuthorizationAgent
from aimpf.dispatcher.mqtt import *

# load_dotenv()

# Set up test fixtures
@pytest.fixture
def retriever(DbSubscriber=Ctxt):
    """
    Fixture for creating a Dispatcher object.

    Parameters
    ----------
    DbSubscriber : class
        The class to use for the DbSubscriber object.

    Returns
    -------
    retriever : DbSubscriber
        The database DbSubscriber object.

    """
    auth_url = "https://api.sandbox.carta.contextualize.us.com"
    agent = AuthorizationAgent(
        os.getenv("CARTA_ROOT_USERNAME"),
        os.getenv("CARTA_ROOT_PASSWORD"),
        url=auth_url)
    host = "t58bbyh0wg.execute-api.us-east-2.amazonaws.com/prod"
    dispatcher_url = f"https://{host}"
    return DbSubscriber(agent, url=dispatcher_url)

# Database information

def test_keywords(retriever):
    """
    Test case for checking the keywords parsed from the endpoint.
    """
    response = retriever.keywords(columns=["dataItemId", "dateTime"], where="assetId=Okuma-4020", start="2020-01-01", end="2022-12-31", limit=10)
    pprint(response)
    assert len(response) > 1

def test_check_service_is_alive(retriever):
    """
    Test case for checking if the service is alive.
    """
    response = retriever.is_alive()
    assert response is True

def test_list_resources(retriever):
    """
    Test case for listing the resources.
    """
    response = retriever.list_resources()
    pprint(response)
    assert len(response['resources']) > 1

def test_list_columns(retriever):
    """
    Test case for listing the columns.
    """
    response = retriever.columns
    pprint(response)
    assert len(response) > 1

# List distinct values of a column

def test_listdistinct_columns(retriever):
    """
    Test case for listing distinct values of a column.
    """
    response = retriever.distinct("assetId")
    pprint(response)
    assert len(response) > 1

def test_listdistinct_column_with_limit(retriever):
    """
    Test case for listing distinct values of a column with a limit.
    """
    response = retriever.distinct("assetId", limit=3)
    pprint(response)
    assert len(response) > 1

def test_listdistinct_column_with_range(retriever):
    """
    Test case for listing distinct values of a column with a range.
    """
    response = retriever.distinct("assetId", start="2020-01-01", end="2024-04-14")
    pprint(response)
    assert len(response) > 1

def test_listdistinct_column_with_range_limit(retriever):
    """
    Test case for listing distinct values of a column with a range and limit.
    """
    response = retriever.distinct("assetId", start="2020-01-01", end="2022-12-31", limit=5)
    pprint(response)
    assert len(response) > 1

def test_listdistinct_columns_with_assetid(retriever):
    """
    Test case for listing distinct values of a column with assetId.
    """
    response = retriever.distinct(columns=["dataItemId", "dateTime"], where="assetId=Okuma-4020", start="2020-01-01", end="2022-12-31", limit=10)
    pprint(response)
    assert len(response) > 1

# Count records

def test_count(retriever):
    """
    Test case for counting records.
    """
    response = retriever.count()
    pprint(response)
    assert response == 2731

def test_count_with_assetid(retriever):
    """
    Test case for counting records with assetId.
    """
    response = retriever.count(where="assetId=Okuma-4020")
    pprint(response)
    assert response == 200

def test_count_with_assetid_range(retriever):
    """
    Test case for counting records with assetId and range.
    """
    response = retriever.count(where="assetId=Okuma-4020", start="2020-01-01", end="2022-12-31")
    pprint(response)
    assert response == 200

# List records

def test_list_with_limit(retriever):
    """
    Test case for listing records with a limit.
    """
    response = retriever.list(limit=50)
    pprint(response)
    assert len(response) == 50

def test_list_with_from_limit(retriever):
    """
    Test case for listing records with a from and limit.
    """
    response = retriever.list(start="2022-09-30T17:53:00", limit=15)
    pprint(response)
    assert len(response) == 15

def test_list_with_to_limit(retriever):
    """
    Test case for listing records with a to and limit.
    """
    response = retriever.list(end="2022-12-31", limit=15)
    pprint(response)
    assert len(response) == 15

def test_list_with_assetid_limit(retriever):
    """
    Test case for listing records with assetId and limit.
    """
    response = retriever.list(where="assetId=Okuma-4020", limit=20)
    pprint(response)
    assert len(response) == 20

def test_list_with_assetid_range_limit(retriever):
    """
    Test case for listing records with assetId, range, and limit.
    """
    response = retriever.list(where="assetId=Okuma-4020", start="2022-08-16", end="2022-12-31", limit=10)
    pprint(response)
    assert len(response) == 10

def test_list_with_id_dataitemid_limit(retriever):
    """
    Test case for listing records with id, dataItemId, and limit.
    """
    response = retriever.list(where="Id<=1024,dataItemId=Heartbeat", limit=10)
    pprint(response)
    assert len(response) == 10

